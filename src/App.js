import React from 'react';
import { NavbarComponent } from 'components';
import {
  AboutUsContainer,
  ChefContainer,
  FindUsContainer,
  FooterContainer,
  GalleryContainer,
  HeaderContainer,
  IntroContainer,
  LaurelsContainer,
  MenuContainer
} from 'containers';
import './App.css';

const App = () => {
  return (
    <div>
      <NavbarComponent />
      <HeaderContainer />
      <AboutUsContainer />
      <MenuContainer />
      <ChefContainer />
      <IntroContainer />
      <LaurelsContainer />
      <GalleryContainer />
      <FindUsContainer />
      <FooterContainer />
  </div>
  );
};

export default App;
