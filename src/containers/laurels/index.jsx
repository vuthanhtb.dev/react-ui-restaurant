import React from 'react';
import { AwardCardComponent, SubHeadingComponent } from 'components';
import { data, images } from 'constants';
import './index.css';

const Laurels = () => {
  return (
    <div className="app__bg app__wrapper section__padding" id="awards">
    <div className="app__wrapper_info">
      <SubHeadingComponent title="Awards & recognition" />
      <h1 className="headtext__cormorant">Our Laurels</h1>

      <div className="app__laurels_awards">
        {data.awards.map((award) => <AwardCardComponent award={award} key={award.title} />)}
      </div>
    </div>

    <div className="app__wrapper_img">
      <img src={images.laurels} alt="laurels_img" />
    </div>
  </div>
  );
};

export default Laurels;
